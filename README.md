### Harbour MTG Search
Sailfish OS Magic the Gathering Card Search App


### Screenshots

<img src="https://gitlab.com/Simerax/harbour-mtg-search/-/raw/main/screenshots/Bildschirmfoto_20210928_001.png" height="480">
<img src="https://gitlab.com/Simerax/harbour-mtg-search/-/raw/main/screenshots/Bildschirmfoto_20210928_002.png" height="480">
<img src="https://gitlab.com/Simerax/harbour-mtg-search/-/raw/main/screenshots/Bildschirmfoto_20210928_003.png" height="480">

