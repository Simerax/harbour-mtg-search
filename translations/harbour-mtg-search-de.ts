<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>About</name>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Source Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>License</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>API Backend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>More on how to use the search can be found here: https://www.scryfall.com/docs/syntax</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This App is NOT affiliated with Wizards of the Coast LLC or Scryfall LLC. However this App would not be possible without the awesome scryfall API!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Additional info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Local Database Path</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AppConfiguration</name>
    <message>
        <source>Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use Multilingual Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Image Cache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This App downloads image data and then stores it locally to save bandwidth. If you have problems with outdated/broken images you can clear the cache.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Clearing Cache...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Images deleted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Currency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Used for price data of individual cards</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>USD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Euro</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Card-Data Cache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Card Data gets automatically updated when older than 24 Hours. A search also updates the data of all found cards. If you think the card Data is out of date you can clear the cache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>cached cards deleted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Interval Card-Updater</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The Card-Updater Interval changes how quickly the card-data is serialized to the database (Card-Data Cache). This update process is done when you run a card search. If your search is laggy try increasing the interval or disable it by setting it to 0</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BasicSearch</name>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Card Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No cards :(</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Favorites</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CardDetail</name>
    <message>
        <source>Rulings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Rarity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Artist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Legalities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Standard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pioneer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Modern</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Legacy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Vintage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Brawl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Historic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pauper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Penny</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Commander</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add to Favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove from Favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Price</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Price Foil</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CardRulings</name>
    <message>
        <source>Rulings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No Rulings available</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CopyArea</name>
    <message>
        <source>Copied</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>MTG-Search</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FavoritesPage</name>
    <message>
        <source>Favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Removed favorite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No favorites added</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Clear Favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>All Favorites removed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
