import QtQuick 2.0
import "../components"

Item {

    Database {
        id: database
    }

    function initDB() {
        var db = database.getConn()
        db.transaction(function(tx){
            //tx.executeSql('DROP TABLE IF EXISTS appconf');
            try {
                tx.executeSql('CREATE TABLE IF NOT EXISTS appconf(key TEXT, value TEXT)')
            } catch(err) {
                console.log("Error creating table in database: " + err)
            }
        })
    }

    function updateConfigValue(key, value) {
        var db = database.getConn()
        db.transaction(function(tx){
            var results = tx.executeSql('SELECT * FROM appconf WHERE key=?',[key])
            if(results.rows.length === 0) {
                tx.executeSql('INSERT INTO appconf values(?,?)', [key, value])
            } else {
                tx.executeSql('UPDATE appconf SET value=? WHERE key=?',[value, key])
            }
        })
    }

    function initConfigValue(key, value) {
        var current = getConfigValue(key)
        if(current === undefined) {
            updateConfigValue(key, value)
        }
    }

    function getConfigValueDefault(key, defaultValue) {
        var value = getConfigValue(key)
        if(value === undefined) {
            return defaultValue
        }
        return value
    }

    function getConfigValue(key) {
        var config = getConfiguration()
        return config[key]
    }

    function getConfiguration() {
        initDB()
        var db = database.getConn()

        var config = {}

        db.transaction(function(tx){
            var rs = tx.executeSql('SELECT * from appconf')
            for(var i = 0; i < rs.rows.length; i++) {
                config[rs.rows.item(i).key] = rs.rows.item(i).value
            }
        })
        return config
    }
}
