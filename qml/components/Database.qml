import QtQuick 2.0
import QtQuick.LocalStorage 2.0
import Sailfish.Silica 1.0

Item {

    function getPath() {
        return StandardPaths.data + "/harbour-mtg-search.db"
    }

    function getConn() {
        return LocalStorage.openDatabaseSync(getPath())
    }

}
