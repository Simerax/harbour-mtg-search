import QtQuick 2.0
import Sailfish.Silica 1.0

Item {
    property string copyValue: parent.value
    property bool clip: false
    anchors.fill: parent

    function doCopy() {
        Clipboard.text = copyValue
        notice.text = qsTr("Copied")
        notice.show()
    }

    MouseArea {
        width: parent.width
        height: parent.height
        onPressAndHold: {
            doCopy()
        }
        clip: parent.clip
    }

    Notice {
        id: notice
        text: ""
    }
}
