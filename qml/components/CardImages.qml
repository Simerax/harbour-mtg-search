import QtQuick 2.0

Item {
    ScryfallAPI {
        id: scryfallAPI
    }

    Database {
        id: database
    }

    function initDB() {
        var db = database.getConn()
        db.transaction(function(tx){
            try {
                //tx.executeSql('DROP TABLE IF EXISTS card_images');
                tx.executeSql('CREATE TABLE IF NOT EXISTS card_images(cardID TEXT, imageStyle TEXT, imageData TEXT, UNIQUE(cardID, imageStyle))')
            } catch(err) {
                console.log("Error creating card_images in database: " + err)
            }
        })
    }

    function clearCache() {
        initDB()
        var rowsAffected = 0
        var db = database.getConn()
        db.transaction(function(tx){
            var rs = tx.executeSql('DELETE FROM card_images')
            rowsAffected = rs.rowsAffected
        })
        return rowsAffected
    }

    function getLargeImage(cardID, callback) {
        getImage(cardID, "large", callback)
    }

    function getBorderCropImage(cardID, callback) {
        getImage(cardID, "border_crop", callback)
    }

    function getImage(cardID, imageStyle, callback) {
        console.log("CardImages.getImage:", cardID, imageStyle)
        initDB()
        var db = database.getConn()
        db.transaction(function(tx){
            var results = tx.executeSql('SELECT imageData FROM card_images WHERE cardID=? AND imageStyle=?',[cardID, imageStyle])

            if(results.rows.length > 0) {
                console.log("CardImages.getImage - found local image")
                callback(results.rows.item(0).imageData)
            } else {
                console.log("CardImages.getImage - downloading remote image")
                scryfallAPI.downloadCardImage(cardID, imageStyle, function(image){
                    console.log("CardImages.getImage - got remote image")
                    db.transaction(function(tx){
                        tx.executeSql('INSERT INTO card_images values(?,?,?)', [cardID, imageStyle, image])
                    })
                    callback(image)
                })
            }
        })
    }
}
