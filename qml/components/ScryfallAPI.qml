import QtQuick 2.0

Item {
    function cardSearch(query, multilingual, callback) {
        var xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function() {
            if(xhr.readyState === XMLHttpRequest.DONE) {
                if(xhr.status === 0 || (xhr.status >= 200 && status < 400)) {
                    var response = JSON.parse(xhr.responseText);
                    if(response.object !== "error") {
                        callback(response);
                    }
                }
            }
        }
        var url = 'https://api.scryfall.com/cards/search?q='+encodeURIComponent(query) + '&include_multilingual='+multilingual
        xhr.open('GET', url)
        xhr.send('')
    }

    function cardLookup(cardUUID, callback) {
        var xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function() {
            if(xhr.readyState === XMLHttpRequest.DONE) {
                if(xhr.status === 0 || (xhr.status >= 200 && status < 400)) {
                    var response = JSON.parse(xhr.responseText);
                    if(response.object !== "error") {
                        callback(response);
                    }
                }
            }
        }
        var url = 'https://api.scryfall.com/cards/'+cardUUID
        xhr.open('GET', url)
        xhr.send('')
    }

    function downloadCardImage(cardUUID, imageStyle, callback) {
        cardLookup(cardUUID, function(cardData){
            var xhr = new XMLHttpRequest()
            xhr.responseType = 'arraybuffer'
            xhr.onreadystatechange = function() {
                if(xhr.readyState === XMLHttpRequest.DONE) {
                    if(xhr.status === 0 || (xhr.status >= 200 && status < 400)) {
                        var response = new Uint8Array(xhr.response)
                        var raw = ""
                        for(var i = 0; i < response.byteLength; i++){
                            raw += String.fromCharCode(response[i])
                        }

                        //FROM https://cdnjs.cloudflare.com/ajax/libs/Base64/1.0.1/base64.js
                        function base64Encode (input) {
                            var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
                            var str = String(input);
                            for (
                                // initialize result and counter
                                var block, charCode, idx = 0, map = chars, output = '';
                                str.charAt(idx | 0) || (map = '=', idx % 1);
                                output += map.charAt(63 & block >> 8 - idx % 1 * 8)
                                ) {
                                charCode = str.charCodeAt(idx += 3/4);
                                if (charCode > 0xFF) {
                                    throw new Error("Base64 encoding failed: The string to be encoded contains characters outside of the Latin1 range.");
                                }
                                block = block << 8 | charCode;
                            }
                            return output;
                        }
                        var image = 'data:image/png;base64,' + base64Encode(raw);
                        callback(image)
                    }
                }
            }
            var url = cardData.image_uris[imageStyle]
            xhr.open('GET', url)
            xhr.send('')
        })
    }
}
