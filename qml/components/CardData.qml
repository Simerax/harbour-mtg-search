import QtQuick 2.0


Item {
    ScryfallAPI {
        id: scryfallAPI
    }

    Database {
        id: database
    }

    function initDB() {
        var db = database.getConn()
        db.transaction(function(tx){
            try {
                //tx.executeSql('DROP TABLE IF EXISTS card_data');
                tx.executeSql('CREATE TABLE IF NOT EXISTS card_data(cardID TEXT, data TEXT, timestamp INTEGER, UNIQUE(cardID))')
            } catch(err) {
                console.log("Error creating card_data in database: " + err)
            }
        })
    }

    function clearCache() {
        initDB()
        var rowsAffected = 0
        var db = database.getConn()
        db.transaction(function(tx){
            var rs = tx.executeSql('DELETE FROM card_data')
            rowsAffected = rs.rowsAffected
        })
        return rowsAffected
    }

    function updateCardData(cardID, data) {
        var dataStringified = JSON.stringify(data)
        initDB()
        var db = database.getConn()
        db.transaction(function(tx){
            var results = tx.executeSql('SELECT * FROM card_data WHERE cardID=?',[cardID])
            if(results.rows.length > 0) {
                tx.executeSql('UPDATE card_data SET data=?, timestamp=? WHERE cardID=?',[dataStringified, Date.now(), cardID])
            } else {
                tx.executeSql('INSERT INTO card_data values(?,?,?)', [cardID, dataStringified, Date.now()])
            }
        })
    }

    function getCardData(cardID, callback) {
        console.log("CardData.getCardData:", cardID)
        initDB()
        var db = database.getConn()
        db.transaction(function(tx){
            var results = tx.executeSql('SELECT data, timestamp FROM card_data WHERE cardID=?',[cardID])

            if(results.rows.length > 0) {
                console.log("CardData.getCardData - found local data")

                var recordAge = Date.now() - results.rows.item(0).timestamp
                var ONE_DAY = 3600 * 24 * 1000 // in milliseconds
                if(recordAge > ONE_DAY) {
                    console.log("CardData.getCardData - local data out of date...updating")
                    scryfallAPI.cardLookup(cardID, function(cardData){
                        db.transaction(function(tx){
                            tx.executeSql('UPDATE card_data SET data=?, timestamp=? WHERE cardID=?',[JSON.stringify(cardData), Date.now(), cardID])
                        })
                        callback(cardData)
                    })
                } else {
                    callback(JSON.parse(results.rows.item(0).data))
                }

            } else {
                console.log("CardData.getCardData - downloading data")
                scryfallAPI.cardLookup(cardID, function(cardData){
                    db.transaction(function(tx){
                        tx.executeSql('INSERT INTO card_data values(?,?,?)', [cardID, JSON.stringify(cardData), Date.now()])
                    })
                    callback(cardData)
                })
            }
        })
    }
}
