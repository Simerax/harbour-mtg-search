import QtQuick 2.0
import Sailfish.Silica 1.0


Row {
    property string format
    property bool legal

    anchors.centerIn: parent.Center

    Component.onCompleted: {
        console.log("loading image")
        if(legal) {
            legalityIcon.source = "image://theme/icon-m-accept"
        } else {
            legalityIcon.source = "image://theme/icon-m-cancel"
        }
    }
    spacing: Theme.paddingMedium
    Icon {
        id: legalityIcon
        anchors.verticalCenter: parent.verticalCenter
        height: Theme.fontSizeSmall
        fillMode: Image.PreserveAspectFit
    }

    Label {
        text: format
        font.pixelSize: Theme.fontSizeSmall
        anchors.verticalCenter: parent.verticalCenter
        color: palette.highlightColor
    }
}
