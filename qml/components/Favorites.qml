import QtQuick 2.0

Item {

    ScryfallAPI {
        id: scryfallAPI
    }
    Database {
        id: database
    }

    function initDB() {
        var db = database.getConn()
        db.transaction(function(tx){
            try {
                //tx.executeSql('DROP TABLE IF EXISTS favorites');
                tx.executeSql('CREATE TABLE IF NOT EXISTS favorites(cardID TEXT)')
            } catch(err) {
                console.log("Error creating favorites in database: " + err)
            }
        })
    }

    function iterateFavorites(callback) {
        initDB()
        var db = database.getConn()
        db.transaction(function(tx){
            var rs = tx.executeSql('SELECT * FROM favorites')
            for(var i = 0; i < rs.rows.length; i++) {
                callback(rs.rows.item(i))
            }
        })
    }

    function isFavorite(cardID) {
        initDB()
        var isFav = false
        var db = database.getConn()
        db.transaction(function(tx){
            var results = tx.executeSql('SELECT * FROM favorites WHERE cardID=?',[cardID])
            if(results.rows.length > 0) {
                isFav = true
            }
        })
        return isFav
    }

    function removeAllFavorites() {
        console.log("Favorites.clearFavorites()")
        initDB()
        var db = database.getConn()
        db.transaction(function(tx){
            tx.executeSql('DELETE FROM favorites')
        })
    }

    function removeFavorite(cardID) {
        console.log("Favorites.removeFavorite() - ", cardID)
        initDB()
        var db = database.getConn()
        db.transaction(function(tx){
            tx.executeSql('DELETE FROM favorites WHERE cardID=?',[cardID])
        })
    }

    function addFavorite(cardID) {
        initDB()
        var db = database.getConn()
        db.transaction(function(tx){
            var results = tx.executeSql('SELECT * FROM favorites WHERE cardID=?',[cardID])
            if(results.rows.length === 0) {
                tx.executeSql('INSERT INTO favorites values(?)', [cardID])
            }
        })
    }
}
