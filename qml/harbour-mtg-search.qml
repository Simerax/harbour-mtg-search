import QtQuick 2.0
import Sailfish.Silica 1.0
import "pages"
import "config"

ApplicationWindow {
    initialPage: Component { BasicSearch { } }
    //initialPage: Component { Favorites{ } }
    cover: Qt.resolvedUrl("cover/CoverPage.qml")
    allowedOrientations: defaultAllowedOrientations

    AppConf {
        id: appConf
    }

    Component.onCompleted: {
        appConf.initConfigValue("currency", "USD")
        appConf.initConfigValue("basic_search_update_cards_timer", 1000)
    }
}
