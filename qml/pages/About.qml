import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Page {
    id: page

    Database {
        id: db
    }

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent
        // Tell SilicaFlickable the height of its content.
        contentHeight: column.height

        // Place our content in a Column.  The PageHeader is always placed at the top
        // of the page, followed by our content.
        Column {
            id: column

            width: page.width
            spacing: Theme.paddingSmall
            PageHeader {
                title: qsTr("About")
            }
            DetailItem {
                label: qsTr("Source Code")
                value: "https://www.gitlab.com/Simerax/harbour-mtg-search"

                CopyArea {
                    width: parent.width
                    height: parent.height
                    copyValue: parent.value
                }
            }
            DetailItem {
                label: qsTr("License")
                value: "MIT"
            }
            DetailItem {
                label: qsTr("API Backend")
                value: "https://www.api.scryfall.com"
                CopyArea {
                    width: parent.width
                    height: parent.height
                    copyValue: parent.value
                }
            }
            TextArea {
                text: qsTr("This App is NOT affiliated with Wizards of the Coast LLC or Scryfall LLC. However this App would not be possible without the awesome scryfall API!")
                readOnly: true
                font.pixelSize: Theme.fontSizeSmall
            }
            LinkedLabel {
                x: Theme.horizontalPageMargin
                width: parent.width
                color: highlighted ? Theme.highlightColor : Theme.primaryColor
                plainText: qsTr("More on how to use the search can be found here: https://www.scryfall.com/docs/syntax")
                linkColor: Theme.highlightColor
                font.pixelSize: Theme.fontSizeSmall
            }

            SectionHeader {
                text: qsTr("Additional info")
            }

            DetailItem {
                label: qsTr("Local Database Path")
                value: db.getPath()

                CopyArea {
                    width: parent.width
                    height: parent.height
                    copyValue: parent.value
                }
            }
        }
    }
}
