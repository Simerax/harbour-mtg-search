import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Page {
    id: page
    property string cardID

    CardImages {
        id: cardImages
    }



    allowedOrientations: Orientation.All


    SilicaFlickable {
        anchors.fill: parent

        contentHeight: column.height

        Column {
            id: column
            Image {
                id: cardImage
                asynchronous: true
                //source: imageURL
                width: page.width
                height: page.height
                fillMode: Image.PreserveAspectFit

                BusyIndicator {
                    size: BusyIndicatorSize.Medium
                    anchors.centerIn: cardImage
                    running: cardImage.status != Image.Ready
                }

                Component.onCompleted: {
                    cardImages.getBorderCropImage(cardID, function(image){
                        cardImage.source = image
                    })
                }
            }
        }
    }
}
