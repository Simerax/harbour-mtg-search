import QtQuick 2.0
import Sailfish.Silica 1.0
import "../config"
import "../components"

Page {
    id: page

    ScryfallAPI {
        id: scryfallAPI
    }

    Favorites {
        id: favorites
    }

    CardImages {
        id: cardImages
    }

    CardData {
        id: cardData
    }

    SilicaGridView {
        id: grid
        anchors.fill: parent

        cellWidth: width/3
        cellHeight: cellWidth * 1.396 // a magic card is roughly 8.8cm * 6.3cm this means width * 1.396 is roughly the height

        RemorsePopup { id: remorse }


        Component.onCompleted: {
            favorites.iterateFavorites(function(fav){
                cardModel.append({"cardID": fav.cardID})
            })
        }

        PullDownMenu {
            MenuItem {
                text: qsTr("Clear Favorites")
                onClicked: {
                    remorse.execute(qsTr("All Favorites removed"), function(){
                        grid.model.clear()
                        favorites.removeAllFavorites()
                    })
                }
            }
        }


        header: Column {
            width: parent.width
            PageHeader {
                title: qsTr("Favorites")
            }
        }

        model: ListModel{
            id: cardModel
        }
        delegate: GridItem {
            Image {
                id: cardImage
                width: grid.cellWidth
                height: grid.cellHeight * 0.95 // we want some space between the cards
                anchors.centerIn: parent
                fillMode: Image.PreserveAspectFit
                //source: image

                BusyIndicator {
                    size: BusyIndicatorSize.Medium
                    anchors.centerIn: cardImage
                    running: cardImage.status != Image.Ready
                }
            }

            Component.onCompleted: {
                cardImages.getBorderCropImage(cardID, function(image){
                    cardImage.source = image
                })
            }

            onClicked: {
                busyCardDetail.running = true

                cardData.getCardData(cardID, function(data){
                    busyCardDetail.running = false
                    pageStack.push(Qt.resolvedUrl("CardDetail.qml"),{
                                       cardData: data
                                   })
                })
            }

            function removeFavorite() {
                remorseAction(qsTr("Removed favorite"), function(){
                    favorites.removeFavorite(cardID)
                    grid.model.remove(index)
                })
            }

            // This indicator is show when the the card is clicked to indicate that the detail page is being loaded
            // since this actually calls out to the Scryfall API
            BusyIndicator {
                id: busyCardDetail
                size: BusyIndicatorSize.Medium
                anchors.centerIn: cardImage
                running: false
            }


            menu: Component {
                ContextMenu {
                    MenuItem {
                        text: qsTr("Remove")
                        onClicked: {
                            removeFavorite()
                        }
                    }
                }
            }
        }

        ViewPlaceholder {
            anchors.centerIn: parent
            enabled: grid.count === 0
            hintText: qsTr("No favorites added")
        }
    }
}
