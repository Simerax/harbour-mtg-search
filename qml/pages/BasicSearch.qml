import QtQuick 2.0
import Sailfish.Silica 1.0
import "../config"
import "../components"

Page {

    property bool searching

    AppConf {
        id: appConf
    }

    ScryfallAPI {
        id: scryfallAPI
    }

    CardData {
        id: cardData
    }


    Timer {
        property string stringifiedResults
        property int result_index: 0
        id: cardDataUpdateWorker
        interval: appConf.getConfigValue("basic_search_update_cards_timer")
        repeat: true
        running: false
        onTriggered: {
            if(interval === 0) {
                cardDataUpdateWorker.stop()
                return
            }

            var results = JSON.parse(stringifiedResults)
            var d = results[result_index]
            cardData.updateCardData(d.id, d)
            result_index++
            if(result_index >= results.length) {
                result_index = 0
                results = null
                cardDataUpdateWorker.stop()
            }
        }
    }

    function updateResults(data) {
        searchResultsModel.clear()
        data.data.forEach(function(value, index){
            // if we run a multilingual search we should display the name in that language
            var name = value.name
            if(value.hasOwnProperty('printed_name')) {
                name = value.printed_name
            }
            //cardData.updateCardData(value.id, value)
            searchResultsModel.append({"name": name, "type": value.type_line, "data_json": JSON.stringify(value)})
        })
        cardDataUpdateWorker.stop()
        cardDataUpdateWorker.interval = appConf.getConfigValue("basic_search_update_cards_timer")
        cardDataUpdateWorker.stringifiedResults = JSON.stringify(data.data)
        cardDataUpdateWorker.start()
        busyIndicator.enabled = false
    }

    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    SilicaListView {
        width: page.width
        anchors.fill: parent
        id: searchView

        PullDownMenu {
            MenuItem {
                text: qsTr("About")
                onClicked: pageStack.animatorPush(Qt.resolvedUrl("About.qml"))
            }
            MenuItem {
                text: qsTr("Favorites")
                onClicked: pageStack.animatorPush(Qt.resolvedUrl("FavoritesPage.qml"))
            }
            MenuItem {
                text: qsTr("Configuration")
                onClicked: pageStack.animatorPush(Qt.resolvedUrl("AppConfiguration.qml"))
            }
        }

        ListModel {
            id: searchResultsModel
        }
        model: searchResultsModel

        header: Column {
            width: parent.width
            PageHeader {
                title: qsTr("Card Search")
            }
            SearchField {
                id: searchField
                width: parent.width
                EnterKey.onClicked: {
                    busyIndicator.running = true
                    searchResultsModel.clear()
                    var multilingual = appConf.getConfigValue("multilingual")
                    scryfallAPI.cardSearch(searchField.text, multilingual, updateResults)
                    //cardSearch(searchField.text, updateResults)
                    busyIndicator.running = false
                }
                EnterKey.enabled: searchField.text.length > 0
            }
        }

        delegate: ListItem {
            id: delegate
            contentHeight: Theme.itemSizeMedium
            Label {
                id: labelheader
                x: Theme.horizontalPageMargin
                text: name
            }

            Label {
                x: Theme.horizontalPageMargin
                font.pixelSize: Theme.fontSizeSmall
                anchors.top: labelheader.bottom
                anchors.verticalCenter: parent.verticalCenter
                text: type
            }

            onClicked: {
                pageStack.push(Qt.resolvedUrl("CardDetail.qml"),{
                                   cardData: JSON.parse(data_json)
                               })
            }
        }
        VerticalScrollDecorator {}

        BusyIndicator {
            id: busyIndicator
            size: BusyIndicatorSize.Large
            anchors.centerIn: parent
            running: false
        }

        ViewPlaceholder {
            anchors.centerIn: parent
            enabled: searchView.count === 0
            hintText: qsTr("No cards :(")
        }
    }
}
