import QtQuick 2.6
import Sailfish.Silica 1.0
import "../config"
import "../components"

Page {
    id: page

    AppConf {
        id: appConf
    }

    CardImages {
        id: cardImages
    }
    CardData {
        id: cardData
    }

    BusyIndicator {
        id: busyIndicatorClearCache
        running: false
        size: BusyIndicatorSize.Medium
        anchors.centerIn: parent
    }

    RemorsePopup {
        id: remorse
    }

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent

        // Tell SilicaFlickable the height of its content.
        contentHeight: column.height

        // Place our content in a Column.  The PageHeader is always placed at the top
        // of the page, followed by our content.
        Column {
            id: column

            width: page.width
            spacing: Theme.paddingLarge
            PageHeader {
                title: qsTr("Configuration")
            }

            TextSwitch {
                text: qsTr("Use Multilingual Search")
                checked: appConf.getConfigValue("multilingual") === "true"
                onCheckedChanged: {
                    appConf.updateConfigValue("multilingual", checked ? "true" : "false")
                }
            }

            ComboBox {
                id: currencySetting
                label: qsTr("Currency")
                description: qsTr("Used for price data of individual cards")
                menu: ContextMenu {
                    MenuItem {
                        text: qsTr("USD")
                        onClicked: {
                            appConf.updateConfigValue("currency", "USD")
                        }
                    }
                    MenuItem {
                        text: qsTr("Euro")
                        onClicked: {
                            appConf.updateConfigValue("currency", "Euro")
                        }
                    }
                }
                Component.onCompleted: {
                    currencySetting.up
                    // There has to be a better way to do this...but im lazy and it works :)
                    var currency = appConf.getConfigValueDefault("currency", "USD")
                    if(currency === "USD") {
                        currencySetting.currentIndex = 0
                    } else {
                        currencySetting.currentIndex = 1
                    }
                }
            }

            Slider {
                id: updateIntervalSetting
                label: qsTr("Interval Card-Updater")
                minimumValue: 0
                maximumValue: 2000
                stepSize: 200
                value: appConf.getConfigValue("basic_search_update_cards_timer")
                width: parent.width
                valueText: value + " ms"

                Component.onDestruction: {
                    appConf.updateConfigValue("basic_search_update_cards_timer", updateIntervalSetting.value)
                }
            }
            TextArea {
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                x: Theme.horizontalPageMargin
                text: qsTr("The Card-Updater Interval changes how quickly the card-data is serialized to the database (Card-Data Cache). This update process is done when you run a card search. If your search is laggy try increasing the interval or disable it by setting it to 0")
                readOnly: true
                font.pixelSize: Theme.fontSizeSmall
            }

            SectionHeader {
                text: qsTr("Advanced")
            }

            ValueButton {
                anchors.horizontalCenter: parent.horizontalCenter
                label: qsTr("Image Cache")
                value: qsTr("Clear")
                description: qsTr("This App downloads image data and then stores it locally to save bandwidth. If you have problems with outdated/broken images you can clear the cache.")

                onClicked: {

                    remorse.execute(qsTr("Clearing Cache..."), function(){
                        busyIndicatorClearCache.running = true
                        var numberOfImagesDeleted = cardImages.clearCache()
                        busyIndicatorClearCache.running = false
                        pageNotice.text = numberOfImagesDeleted + " " + qsTr("Images deleted")
                        pageNotice.show()
                    })

                }
            }

            ValueButton {
                anchors.horizontalCenter: parent.horizontalCenter
                label: qsTr("Card-Data Cache")
                value: qsTr("Clear")
                description: qsTr("Card Data gets automatically updated when older than 24 Hours. A search also updates the data of all found cards. If you think the card Data is out of date you can clear the cache")

                onClicked: {
                    remorse.execute("", function(){
                        busyIndicatorClearCache.running = true
                        var rowsDeleted = cardData.clearCache()
                        busyIndicatorClearCache.running = false
                        pageNotice.text = rowsDeleted + " " + qsTr("cached cards deleted")
                        pageNotice.show()
                    })
                }
            }

            Notice {
                id: pageNotice
                text: ""
            }
        }
    }
}
