import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Page {

    property string cardName
    property string cardID
    property bool updatedRulings

    function cardRulings(cardID, callback) {
        var xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function() {
            if(xhr.readyState === XMLHttpRequest.DONE) {
                if(xhr.status === 0 || (xhr.status >= 200 && status < 400)) {
                    var response = JSON.parse(xhr.responseText);
                    callback(response);
                }
            }
        }
        xhr.open("GET", "https://api.scryfall.com/cards/"+cardID+"/rulings")
        xhr.send('')
    }

    function updateRulings(data) {
        rulingsModel.clear()
        data.data.forEach(function(value, index){
            rulingsModel.append({"rulingsText": value.comment})
        })
    }

    id: page

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All


    SilicaListView {
        width: page.width
        anchors.fill: parent
        id: rulingsView

        Component.onCompleted: {
            updatedRulings = false
            cardRulings(cardID, updateRulings)
            updatedRulings = true
        }

        ListModel {
            id: rulingsModel
        }
        model: rulingsModel

        header: Column {
            width: parent.width
            PageHeader {
                title: qsTr("Rulings")
                description: cardName
            }
        }

        delegate: ListItem {
            id: delegate
            anchors.horizontalCenter: parent.horizontalCenter
            //contentHeight: Theme.itemSizeMedium
            height: content.height
            width: page.width

            Column {
                id: content
                height: labelheader.height
                width: parent.width

                TextArea {
                    width: parent.width
                    id: labelheader
                    anchors.horizontalCenter: parent.horizontalCenter
                    x: Theme.horizontalPageMargin
                    text: rulingsText
                    readOnly: true
                    font.pixelSize: Theme.fontSizeSmall

                    onPressAndHold: {
                        // somehow TextArea still gets the event even though CopyArea should be on top of it...
                        copyArea.doCopy()
                    }
                    CopyArea {
                        copyValue: rulingsText
                        id: copyArea
                    }
                }
            }
        }
        VerticalScrollDecorator {}

        BusyIndicator {
            id: busyIndicator
            size: BusyIndicatorSize.Large
            anchors.centerIn: parent
            running: updatedRulings === false
        }

        ViewPlaceholder {
            anchors.centerIn: parent
            enabled: rulingsView.count === 0
            hintText: qsTr("No Rulings available")
        }
    }
}
