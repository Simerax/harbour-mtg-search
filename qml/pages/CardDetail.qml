import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"
import "../config"

Page {
    id: page

    // cardData is the json parsed response from the card search api (https://api.scryfall.com/cards/search)
    // therefore it has the exact same properties like name, type_line, oracle_text, etc.
    property var cardData: ({})


    allowedOrientations: Orientation.All

    Favorites {
        id: favorites
    }

    CardImages {
        id: cardImages
    }

    AppConf {
        id: appConf
    }


    SilicaFlickable {
        anchors.fill: parent

        contentHeight: column.height

        PullDownMenu {
            MenuItem {
                text: qsTr("Rulings")
                onClicked: {
                    pageStack.push(Qt.resolvedUrl("CardRulings.qml"),{
                                       cardID: cardData.id,
                                       cardName: cardData.name
                                   })
                }
            }
            MenuItem {
                id: pullDownItemAddFavorite
                text: qsTr("Add to Favorites")
                enabled: !favorites.isFavorite(cardData.id)
                onClicked: {
                    favorites.addFavorite(cardData.id)     
                    pullDownItemAddFavorite.enabled = false
                    pullDownItemRemoveFavorite.enabled = true

                }
            }
            MenuItem {
                id: pullDownItemRemoveFavorite
                text: qsTr("Remove from Favorites")
                enabled: favorites.isFavorite(cardData.id)
                onClicked: {
                    favorites.removeFavorite(cardData.id)
                    pullDownItemAddFavorite.enabled = true
                    pullDownItemRemoveFavorite.enabled = false
                }
            }
        }

        Column {
            id: column

            width: page.width
            spacing: Theme.paddingLarge
            PageHeader {
                title: cardData.name
                description: cardData.type_line

                CopyArea {
                    copyValue: parent.title + " | " + parent.description
                }
            }

            Image {
                id: cardImage
                anchors.horizontalCenter: parent.horizontalCenter
                height: page.height / 2.5
                asynchronous: true
                width: parent.width / 2 * 1.3
                fillMode: Image.PreserveAspectFit

                BusyIndicator {
                    size: BusyIndicatorSize.Medium
                    anchors.centerIn: cardImage
                    running: cardImage.status != Image.Ready
                }

                Component.onCompleted: {
                    cardImages.getBorderCropImage(cardData.id, function(image){
                        cardImage.source = image
                    })
                }

                MouseArea {
                    width: parent.width
                    height: parent.height
                    onClicked: {
                        pageStack.push(Qt.resolvedUrl("CardImage.qml"),{
                                           cardID: cardData.id
                                       })
                    }
                }
            }

            Column {
                width: parent.width
                DetailItem {
                    label: qsTr("Set")
                    value: cardData.set_name + " ("+cardData.set+")"
                    CopyArea {}
                }
                DetailItem {
                    label: qsTr("Rarity")
                    value: cardData.rarity
                }
                DetailItem {
                    label: qsTr("Artist")
                    value: cardData.artist
                    CopyArea {}
                }

                DetailItem {
                    id: priceData
                    label: qsTr("Price")
                    value: "loading..."
                    Component.onCompleted: {
                        var currency = appConf.getConfigValue("currency")
                        priceData.label = qsTr("Price") + " " + currency

                        if(currency === "USD") {
                            priceData.value = cardData.prices.usd
                        } else {
                            priceData.value = cardData.prices.eur
                        }
                    }
                    CopyArea {}
                }

                DetailItem {
                    id: priceDataFoil
                    Component.onCompleted: {

                        if(cardData.prices.usd_foil !== null) {
                            var currency = appConf.getConfigValue("currency")
                            priceDataFoil.label = qsTr("Price Foil") + " " + currency

                            if(currency === "USD") {
                                priceDataFoil.value = cardData.prices.usd_foil
                            } else {
                                priceDataFoil.value = cardData.prices.eur_foil
                            }
                        }
                    }
                    CopyArea {}
                }

                TextArea {
                    id: oracleText
                    text: cardData.oracle_text
                    readOnly: true
                    font.pixelSize: Theme.fontSizeSmall

                    onPressAndHold: {
                        // somehow TextArea still gets the event even though CopyArea should be on top of it...
                        copyArea.doCopy()
                    }
                    CopyArea {
                        copyValue: cardData.oracle_text
                        id: copyArea
                    }
                }


                SectionHeader {
                    id: legalitySectionHeader
                    text: qsTr("Legalities")
                }


                Grid {
                    columns: 2
                    width: page.width
                    columnSpacing: Theme.paddingMedium
                    x: Theme.horizontalPageMargin

                    CardLegalityItem {
                        format: qsTr("Standard")
                        legal: cardData.legalities.standard === "legal"
                    }
                    CardLegalityItem {
                        format: qsTr("Pioneer")
                        legal: cardData.legalities.pioneer === "legal"
                    }

                    CardLegalityItem {
                        format: qsTr("Modern")
                        legal: cardData.legalities.modern === "legal"
                    }
                    CardLegalityItem {
                        format: qsTr("Legacy")
                        legal: cardData.legalities.legacy === "legal"
                    }

                    CardLegalityItem {
                        format: qsTr("Vintage")
                        legal: cardData.legalities.vintage === "legal"
                    }

                    CardLegalityItem {
                        format: qsTr("Brawl")
                        legal: cardData.legalities.brawl === "legal"
                    }
                    CardLegalityItem {
                        format: qsTr("Historic")
                        legal: cardData.legalities.historic === "legal"
                    }
                    CardLegalityItem {
                        format: qsTr("Pauper")
                        legal: cardData.legalities.pauper === "legal"
                    }
                    CardLegalityItem {
                        format: qsTr("Penny")
                        legal: cardData.legalities.penny === "legal"
                    }
                    CardLegalityItem {
                        format: qsTr("Commander")
                        legal: cardData.legalities.commander === "legal"
                    }
                }




                /*

                CardLegalityItem {
                    format: qsTr("Standard")
                    legal: cardData.legalities.standard === "legal"
                }
                CardLegalityItem {
                    format: qsTr("Commander")
                    legal: cardData.legalities.commander === "legal"
                }
                CardLegalityItem {
                    format: qsTr("Modern")
                    legal: cardData.legalities.modern === "legal"
                }
                CardLegalityItem {
                    format: qsTr("Vintage")
                    legal: cardData.legalities.vintage === "legal"
                }
                CardLegalityItem {
                    format: qsTr("Pauper")
                    legal: cardData.legalities.pauper === "legal"
                }
                CardLegalityItem {
                    format: qsTr("Penny")
                    legal: cardData.legalities.pauper === "legal"
                }
                */
            }
        }
    }
}
